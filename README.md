# ECI4FOAM

External Coupling Interface 4 FOAM ECI4FOAM provides an interface for coupling external tools and software to OpenFOAM.

## Installation

### Dependencies

```bash
# Software library for encryption, decryption, signatures, password hashing, and more.
sudo apt-get install libsodium-dev

# Lightweight header-only library that exposes C++ types in Python and vice versa, mainly to create Python bindings of existing C++ code
sudo apt-get install pybind11-dev

# JSON library
sudo apt-get install nlohmann-json3-dev

# Lightweight messaging kernel based on ZeroMQ
sudo apt-get install libzmq3-dev

sudo apt-get install python3-dev

# C++ Automated Test Cases in Headers
sudo apt-get install catch2

python3 -m pip install oftest

# Export variables for Python.h
export CPATH=/usr/include/python3.10:$CPATH
export LD_LIBRARY_PATH=/usr/lib:$LD_LIBRARY_PATH
```

### Build

Create a Software folder to install ECI4FOAM and all the dependencies.
```bash
cd ~/
mkdir Software
```

If `libzmq` and `cppzmq` are not available using system installation such as `apt`.

From [cppzmq GitHub project](https://github.com/zeromq/cppzmq)

**zmq**
```bash
# or
cd ~/Software
git clone https://github.com/zeromq/libzmq.git
cd libzmq
mkdir build
cd build
cmake ..
make -j4
sudo make -j4 install
```

**cppzmq**
```bash
cd ~/Software
git clone https://github.com/zeromq/cppzmq.git
cd cppzmq
mkdir build
cd build
cmake ..
make -j4
sudo make -j4 install
```

Finally build ECI4FOAM:
```bash
cd ~/Software
git clone https://gitlab.com/foam-for-nuclear/ECI4FOAM.git
cd ECI4FOAM
./Allwmake -j4
```

## Test the package

Execute in the root of the project
```bash
pytest
```

## Troubleshooting

- libcppzmq is not available for Ubuntu22 but available for Ubuntu20 -> need to install manually
- In `src\embeddingPython\Make\options`, might need to adapt -lpython3.10 to the user version



--------------------------------------------------------------------------------
# Legacy

External Coupling Interface 4 FOAM ECI4FOAM provides an interface for coupling external tools and software to OpenFOAM

# Documentation

[Documentation](https://DLR-RY.github.io/ECI4FOAM/)

## Installation

requires OpenFOAM of2212 or higher sourced and installed and Python 3.7+ (conda is highly recommended) 

```
./build-ECI4FOAM.sh # will install conan zmq oftest
```

Note: Older versions are supported on v1.0.1
## Testsuite

install oftest to automatically test OpenFOAM with py.test

```
pip install oftest
```

run the test environment
```
py.test
```

## Build Documentation

The documentation is based on the [Jekyll Documentation Theme](https://idratherbewriting.com/documentation-theme-jekyll/)

```
cd docs
jekyll serve
```